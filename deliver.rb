#!/usr/bin/ruby

# https://blog.joinmastodon.org/2018/06/how-to-implement-a-basic-activitypub-server/

require 'http'
require 'openssl'

document      = File.read(ARGV[0])
date          = Time.now.utc.httpdate
keypair       = OpenSSL::PKey::RSA.new(File.read('private.pem'))
signed_string = "(request-target): post /inbox\ndate: #{date}"
signature     = Base64.strict_encode64(keypair.sign(OpenSSL::Digest::SHA256.new, signed_string))
header        = 'keyId="https://test.exozy.me/api/v1/activitypub/user/test#main-key",headers="(request-target) date",signature="' + signature + '"'

HTTP.headers({ 'Date': date, 'Signature': header })
    .post('https://git.exozy.me/api/v1/activitypub/repo/Ta180m/Hello-world/inbox', body: document)
